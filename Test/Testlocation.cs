using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace SwinTopia;

public class Testlocation{
    [SetUp]
    public void SetUp(){}

    [Test]
    public void Testlocationlocate(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        Item book = new Item(new string[] {"book", "text-book"}, "book", "a book for reading");
        classroom.locationinventory.Put(book);

        matt.changeLocation(classroom);

        Assert.AreEqual(classroom.Locate("book"), book);
    }

    [Test]
    public void TestlocationIdentify(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");

        matt.changeLocation(classroom);

        Assert.AreEqual(classroom.Locate("classroom"), classroom);
    }
}