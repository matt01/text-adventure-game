using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class Test_Inventory{
    [SetUp]
    public void setup(){

    }

    [Test]
    public void TestFindItem(){
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item myapple = new Item(ids, "Pink Lady", "a nice apple");
        myInventory.Put(myapple);

        Assert.IsTrue(myInventory.HasItem("Pink Lady"));
    }

    [Test]
    public void TestNoFindItem(){
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item myapple = new Item(ids, "pink lady", "a nice apple");
        myInventory.Put(myapple);

        Assert.IsFalse(myInventory.HasItem("Royal gala"));
    }

    [Test]
    public void TestFetchItem(){//To check that it returns
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item myapple = new Item(ids, "pink lady", "a nice apple");
        myInventory.Put(myapple);

        Assert.AreEqual(myInventory.Fetch("pink lady"), myapple);
    }

    [Test]
    public void AlsoTestFetchItem(){//To check if it stays in the inventory
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item myapple = new Item(ids, "pink lady", "a nice apple");
        myInventory.Put(myapple);
        myInventory.Fetch("pink lady");

        Assert.IsTrue(myInventory.HasItem("pink lady"));
    }

    [Test]
    public void TestTake(){//To check that it returns
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item myapple = new Item(ids, "pink lady", "a nice apple");
        myInventory.Put(myapple);

        Assert.AreEqual(myInventory.Take("pink lady"), myapple);
    }

    [Test]
    public void AlsoTestTake(){//To check that it does not stay in the inventory
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item myapple = new Item(ids, "pink lady", "a nice apple");
        myInventory.Put(myapple);
        myInventory.Take("pink lady");

        Assert.IsFalse(myInventory.HasItem("pink lady"));
    }

    [Test]
    public void ItemList(){
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        string[] ids2 = new string[] {"Pakam", "pear"};
        Item myapple = new Item(ids, "pink lady", "a nice apple");
        Item mypear = new Item(ids2, "pakam", "a nice pear");
        myInventory.Put(mypear);
        myInventory.Put(myapple);

        Assert.AreEqual(myInventory.ItemList, "\npakam\npink lady");
    }
}