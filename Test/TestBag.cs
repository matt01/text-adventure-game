using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class TestBag{
    [SetUp]
    public void SetUp(){}

    [Test]
    public void TestBagLocatesItems(){
        string[] ids = new string[] {"bag", "This is a bag"};
        string[] ids2 = new string[] {"Pink lady", "apple"};
        Bag myBag = new Bag(ids, "Bag", "This is a bag");
        Item apple = new Item(ids2, "Pink Lady", "apple");
        myBag.Put(apple);

        Assert.AreEqual(apple, myBag.Locate("Pink Lady"));
    }

    [Test]
    public void TestBagLocatesSelf(){
        string[] ids = new string[] {"bag", "This is a bag"};
        Bag myBag = new Bag(ids, "Bag", "This is a bag");

        Assert.AreEqual(myBag, myBag.Locate("bag"));
    }

    [Test]
    public void TestBagLocatesNothing(){
        string[] ids = new string[] {"bag", "This is a bag"};
        Bag myBag = new Bag(ids, "Bag", "This is a bag");

        Assert.IsNull(myBag.Locate("Royal Gala"));
    }

    [Test]
    public void TestBagFullDescription(){
        string[] ids = new string[] {"bag", "This is a bag"};
        Bag myBag = new Bag(ids, "Bag", "This bag can carry things");

        Assert.AreEqual("This bag can carry things\nIt contains:", myBag.FullDescription);
    }

    [Test]
    public void TestBagLocateBag(){
        string[] ids = new string[] {"Bag", "This is a bag"};
        string[] ids2 = new string[] {"BagB", "This is also a bag"};

        Bag BagA = new Bag(ids, "Bag", "This is a bag");
        Bag BagB = new Bag(ids2, "BagB", "This is a bag");

        BagA.Put(BagB);

        Assert.AreEqual(BagB, BagA.Locate("bagb"));
    }

    [Test]
    public void TestBagLocateItemNexttoBag(){
        string[] ids = new string[] {"Bag", "This is a bag"};
        string[] ids2 = new string[] {"BagB", "This is also a bag"};
        string[] ids3 = new string[] {"Pink Lady", "This is an apple"};

        Bag BagA = new Bag(ids, "Bag", "This is a bag");
        Bag BagB = new Bag(ids2, "BagB", "This is a bag");
        Item myapple = new Item(ids3, "Pink Lady", "This is an apple");

        BagA.Put(BagB);
        BagA.Put(myapple);

        Assert.AreEqual(myapple, BagA.Locate("pink lady"));
    }

    [Test]
    public void TestBagCannotLocateBagBItem(){
        string[] ids = new string[] {"Bag", "This is a bag"};
        string[] ids2 = new string[] {"BagB", "This is also a bag"};
        string[] ids3 = new string[] {"Pink Lady", "This is an apple"};

        Bag BagA = new Bag(ids, "Bag", "This is a bag");
        Bag BagB = new Bag(ids2, "BagB", "This is a bag");
        Item myapple = new Item(ids3, "Pink Lady", "This is an apple");

        BagA.Put(BagB);
        BagB.Put(myapple);

        Assert.IsNull(BagA.Locate("pink lady"));
    }
}