using System;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class TestInvalidLook{
    [SetUp]
    public void SetUp(){}

    [Test]
    public void TestWrongLength(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "around"};

        Assert.AreEqual(lookat.Execute(Matt, mystring), "I don't know how to look like that");
    }

    [Test]
    public void TestNoLook(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Hello", "look", "look"};

        Assert.AreEqual(lookat.Execute(Matt, mystring), "Error in look input");
    }

    [Test]
    public void TestLookAtWhat(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"look", "in", "what"};

        Assert.AreEqual(lookat.Execute(Matt, mystring), "What do you want to look at?");
    }
}