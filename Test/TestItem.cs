using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class TestItem{
    [SetUp]
    public void SetUp(){

    }

    [Test]
    public void Test_identifiable(){
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item apple = new Item(ids, "Pink Lady", "a nice apple");
        myInventory.Put(apple);
        Assert.IsTrue(apple.AreYou("pink lady"));
    }

    [Test]
    public void Test_shortDescription(){
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item apple = new Item(ids, "Pink Lady", "a nice apple");
        myInventory.Put(apple);
        Assert.AreEqual(apple.ShortDescription, "Pink Lady (a nice apple)");
        
    }

    [Test]
    public void Test_Description(){
        Inventory myInventory = new Inventory();
        string[] ids = new string[] {"Pink lady", "apple"};
        Item apple = new Item(ids, "Pink Lady", "a nice apple");
        myInventory.Put(apple);
        Assert.AreEqual(apple.FullDescription, "a nice apple");
    }
}