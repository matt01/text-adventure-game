using System;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class TestLook{
    [SetUp]
    public void SetUp(){}

    [Test]
    public void TestLookAtMe(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "inventory"};

        Assert.AreEqual(lookat.Execute(Matt, mystring), "You are the Player\nYour inventory contains:");
    }

    [Test]
    public void TestLookAtGem(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "gem"};
        string[] ids = new string[] {"gem", "ruby"};
        Item gem = new Item(ids, "gem", "a shining ruby");

        Matt.PlayerInventory.Put(gem);

        Assert.AreEqual(lookat.Execute(Matt, mystring), "a shining ruby");
    }

    [Test]
    public void TestLookAtUnk(){
        Player Matt = new Player("me", "inventory");
        location myloc = new location(new string[] {"location"}, "locatin", "testlocation");
        Matt.changeLocation(myloc);
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "gem", "in", "inventory"};

        Assert.AreEqual("I can't find the gem", lookat.Execute(Matt, mystring));
    }

    [Test]
    public void TestLookAtGeminMe(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "gem", "in", "inventory"};
        string[] ids = new string[] {"gem", "ruby"};
        Item gem = new Item(ids, "gem", "a shining ruby");

        Matt.PlayerInventory.Put(gem);

        Assert.AreEqual("a shining ruby", lookat.Execute(Matt, mystring));
    }

    [Test]
    public void TestLookAtGemInBag(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "gem", "in", "bag"};
        string[] ids = new string[] {"gem", "ruby"};
        string[] bagids = new string[] {"bag", "bag for things"};
        Item gem = new Item(ids, "gem", "a shining ruby");

        Bag myBag = new Bag(bagids, "bag", "You can use this to carry things");

        Matt.PlayerInventory.Put(myBag);
        myBag.Put(gem);

        Assert.AreEqual("a shining ruby", lookat.Execute(Matt, mystring));
    }

    [Test]
    public void TestLookAtGemNoBag(){
        Player Matt = new Player("me", "inventory");
        location myloc = new location(new string[] {"location"}, "locatin", "testlocation");
        Matt.changeLocation(myloc);
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "gem", "in", "bag"};
        string[] ids = new string[] {"gem", "ruby"};
        string[] bagids = new string[] {"bag", "bag for things"};
        Item gem = new Item(ids, "gem", "a shining ruby");

        Bag myBag = new Bag(bagids, "bag", "You can use this to carry things");

        myBag.Put(gem);

        Assert.AreEqual("I can't find the bag", lookat.Execute(Matt, mystring));
    }

    [Test]
    public void TestLookAtNoGemInbag(){
        Player Matt = new Player("me", "inventory");
        Look lookat = new Look();
        string[] mystring = new string[] {"Look", "at", "gem", "in", "bag"};
        string[] bagids = new string[] {"bag", "bag for things"};

        Bag myBag = new Bag(bagids, "bag", "You can use this to carry things");

        Matt.PlayerInventory.Put(myBag);

        Assert.AreEqual("I can't find the gem", lookat.Execute(Matt, mystring));
    }

    [Test]
    public void TestLookAtInLocation(){
        Player matt = new Player("matt", "player");
        Look lookat = new Look();
        string[] mystring = new string[] {"look", "at", "book"};

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        Item book = new Item(new string[] {"book", "text-book"}, "book", "a book for reading");
        classroom.locationinventory.Put(book);

        matt.changeLocation(classroom);

        Assert.AreEqual("a book for reading", lookat.Execute(matt, mystring));
    }
}