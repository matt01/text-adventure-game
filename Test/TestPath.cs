using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace SwinTopia;

public class TestPath{
    [SetUp]
    public void Setup(){}

    [Test]
    public void MoveDownPath(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        location library = new location(new string[] {"library", "a place for reading"}, "library", "a place for reading");

        matt.changeLocation(library);

        Path mypath = new Path(new string[] {"classroom", "library", "path"}, classroom, "library-path", "a nice path");
        library.addPath(mypath);

        Move moove = new Move();
        string[] mystring = new string[] {"move", "to", "classroom"};
        string result = moove.Execute(matt, mystring);

        Assert.AreEqual("location changed", result);
    }

    [Test]
    public void Pathmove(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        location library = new location(new string[] {"library", "a place for reading"}, "library", "a place for reading");

        matt.changeLocation(library);

        Path mypath = new Path(new string[] {"classroom", "library", "path"}, classroom, "library-path", "a nice path");
        library.addPath(mypath);

        Move moove = new Move();
        string[] mystring = new string[] {"move", "to", "classroom"};
        string result = moove.Execute(matt, mystring);

        Assert.AreEqual(classroom, matt.Locate("classroom"));
    }

    [Test]
    public void MovebackAndForth(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        location library = new location(new string[] {"library", "a place for reading"}, "library", "a place for reading");

        matt.changeLocation(library);

        Path mypath = new Path(new string[] {"classroom", "library", "path"},library, "library-path", "a nice path");
        library.addPath(mypath);
        classroom.addPath(mypath);

        Move moove = new Move();
        string[] mystring = new string[] {"move", "to", "classroom"};
        string result = moove.Execute(matt, mystring);

        string[] alsomystring = new string[] {"move", "to", "library"};
        string result2 = moove.Execute(matt, alsomystring);

        Assert.AreEqual(library, matt.Locate("library"));
    }
}