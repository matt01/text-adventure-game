using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class TestIdentifiableObject
{
    [SetUp]
    public void Setup()
    {

    }

    [Test]
    public void TestAreYou()
    {
        List<string> are_string = new List<string>() {"Pink Lady", "Apple"};
        identifiableObject apple = new identifiableObject(are_string);
        Assert.IsTrue(apple.AreYou("pink Lady"));
    }

    [Test]
    public void TestNotAreYou(){
        List<string> are_string = new List<string>() {"Pink Lady", "Apple"};
        identifiableObject apple = new identifiableObject(are_string);
        Assert.IsFalse(apple.AreYou("Pear"));
    }

    [Test]
    public void TestCaseSensitive(){
        List<string> are_string = new List<string>() {"Pink Lady", "Apple"};
        identifiableObject apple = new identifiableObject(are_string);
        Assert.IsTrue(apple.AreYou("PINK lAdY"));
    }

    [Test]
    public void TestFirstID(){
        List<string> are_string = new List<string>() {"Pink Lady", "Apple"};
        identifiableObject apple = new identifiableObject(are_string);
        Assert.AreEqual(apple.ID1, "pink lady");
    }

    [Test]
    public void TestFIrstIDnoID(){
        List<string> are_string = new List<string>() {"Pink Lady", "Apple"};
        identifiableObject apple = new identifiableObject(are_string);
        apple.Remove_id();
        Assert.AreEqual(apple.ID1, "");
    }

    [Test]
    public void TestAddID(){
        List<string> are_string = new List<string>() {"Pink Lady", "Apple"};
        identifiableObject apple = new identifiableObject(are_string);
        apple.Add_id("Fruit");
        Assert.IsTrue(apple.AreYou("fruit"));
    }
}