using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Diagnostics.Contracts;

namespace SwinTopia;

public class TestPlayer{
    [SetUp]
    public void setup(){

    }

    [Test]
    public void Test_identifiable_player(){
        Player Matt = new Player("me", "inventory");
        Assert.IsTrue(Matt.AreYou("me"));
    }

    [Test]
    public void AlsoTest_identifiable_player(){
        Player Matt = new Player("me", "inventory");
        Assert.IsTrue(Matt.AreYou("inventory"));
    }

    [Test]
    public void TestLocateSelf(){
        Player Matt = new Player("me", "inventory");
        Assert.AreEqual(Matt, Matt.Locate("me"));
    }

    [Test]
    public void TestLocateNull(){
        Player Matt = new Player("me", "inventory");
        location myloc = new location(new string[] {"location"}, "location", "testlocation");
        Matt.changeLocation(myloc);

        Assert.IsNull(Matt.Locate("banana"));
    }

    [Test]
    public void TestFullDescription(){
        Player Matt = new Player("me", "inventory");
        Assert.AreEqual(Matt.FullDescription, "You are the Player\nYour inventory contains:");
    }

    [Test]
    public void TestLocateInLocation(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        Item book = new Item(new string[] {"book", "text-book"}, "book", "a book for reading");
        classroom.locationinventory.Put(book);

        matt.changeLocation(classroom);

        Assert.AreEqual(matt.Locate("book"), book);
    }

    [Test]
    public void TestChangeLocation(){
        Player matt = new Player("matt", "player");

        location classroom = new location(new string[] {"classroom", "a place for learning"}, "classroom", "a place for learning");
        Item book = new Item(new string[] {"book", "text-book"}, "book", "a book for reading");
        classroom.locationinventory.Put(book);

        matt.changeLocation(classroom);

        location library = new location(new string[] {"library", "a place for reading"}, "library", "a place for reading");
        Item novel = new Item(new string[] {"novel", "book"}, "novel", "It contains a story");
        library.locationinventory.Put(novel);

        matt.changeLocation(library);

        Assert.AreEqual(matt.Locate("novel"), novel);
    }
}