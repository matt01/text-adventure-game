using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Diagnostics.Contracts;
using System.IO;

namespace SwinTopia;

public class TestCommandProcessor{
    [SetUp]
    public void setup(){}

    [Test]
    public void TestExecute(){
        Player p = new Player("Matt", "Player");
        location classroom = new location(new string[] {"classroom", "location"}, "classroom", "You're in a classroom");
        p.changeLocation(classroom);
        CommandProcessor processor = new CommandProcessor(p);
        
        Assert.AreEqual("You're in a classroom\nIt contains:\n\nPaths:", processor.Execute("Look", p));
    }
}