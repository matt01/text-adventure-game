using System;

namespace SwinTopia{
    public class Path : GameObject{
        private location _destination;
        public Path(string[] ids, location destination, string name, string description) : base(ids, name, description){
            _destination = destination;
            string direction = "Towards" + destination.FullDescription;
            base.Add_id(direction);
            _description = description;
        }

        public location destination{
            get{
                return _destination;
            }
        }
    }
}