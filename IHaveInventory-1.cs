using System;

namespace SwinTopia{
    public interface IHaveInventory
    {
        GameObject Locate(string id);
        public String Name{
            get;
        }
    }
}