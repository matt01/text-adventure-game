using System;
using System.Collections.Generic;

namespace SwinTopia{
    public abstract class Command : identifiableObject{
        public Command(string[] ids) : base(new List<string>(ids)){

        }

        public abstract string Execute(Player p, string[] text);
    }
}