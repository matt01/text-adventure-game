using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class CommandProcessor : identifiableObject{
        private Command[] _Commands = new Command[]{new Move(), new Look()};
        private Player _p1 = null!;
        public CommandProcessor(Player p1) : base(new List<string>() {"Command-Processor"}){
            _p1 = p1;
        }

        public string Execute(string input, Player p){//should be private, left public for TestExecute
            string[] words = input.Split(' ');
            foreach(Command c in _Commands){
                if(c.AreYou(words[0])){
                    return c.Execute(p, words);
                }
            }

            return "No command matches that identifier";
        }
    }
}