using System;

namespace SwinTopia{
    public class Move : Command{
        private static string[] ids = new string[] {"Move", "MoveCommand"};

        public Move() : base(ids){

        }

        public override string Execute(Player p, string[] text)
        {
            if(text.Length != 2 && text.Length != 3){
                return "error in location input";
            }

            Path mypath = p.playerlocation.FindDestination(text[2]);

            try{
                if(mypath.destination.Name == text[2]){
                    p.changeLocation(mypath.destination);
                    return "location changed";
                }
            }
            catch{
                return "error in location change, destination is null";
            }
                
            return "error in location change";
        }
    }
}