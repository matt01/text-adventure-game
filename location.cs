using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class location : GameObject, IHaveInventory{
        private Inventory _Inventory = new Inventory();
        private List<Path> _Paths = new List<Path>();
        public location(string[] ids, string name, string desc) : base(ids, name, desc){
            
        }

        public Inventory locationinventory{
            get{
                return _Inventory;
            }
        }

        public Path FindDestination(string s){
            foreach(Path p in this._Paths){
                if(p.AreYou(s) == true){
                    return p;
                }
            }
            return null!;
        }

        public GameObject Locate(string id){
            if(this.AreYou(id) == true){
                return this;
            }
            return _Inventory.Fetch(id);
        }

        public override string FullDescription{
            get{
                string things = _Inventory.ItemList;
                string paths = this.ListPaths;
                return _description + "\nIt contains:" + things + "\n" + paths;
            }
        }

        public string Name{
            get{
                return _name;
            }
        }

        public void addPath(Path p){
            _Paths.Add(p);
        }
        public string ListPaths{
            get{
                string result = "\nPaths:";
                foreach(Path p in _Paths){
                    result = result + "\n" + p.name;
                }
                return result;
            }
        }

        public GameObject FindPath(string id){
            foreach(Path p in _Paths){
                if(p.AreYou(id)){
                    return p;
                }
            }
            return null!;
        }
    }
}