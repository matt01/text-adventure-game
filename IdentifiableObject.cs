using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class identifiableObject{
        protected List<string> _identifiers = new List<string>();

        public identifiableObject(List<string> names){
            foreach(string s in names){
                _identifiers.Add(s.ToLower());
            }
        }

        public bool AreYou(string id){
                foreach(string s in _identifiers){
                    if(id.Equals(s, StringComparison.CurrentCultureIgnoreCase) == true){
                        return true;
                    }
                }
            return false;
        }

        public string ID1{
            get{
                if(_identifiers.Any()){
                    return _identifiers[0];
                }
                return "";
            }
        }

        public void Add_id(string new_name){
            _identifiers.Add(new_name.ToLower());
        }

        public void Remove_id(){
            _identifiers.Clear();
        }
    }
}