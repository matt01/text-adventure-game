﻿using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class Program{
        public static void Main(){
            Console.WriteLine("What is your name?");
            string name = Console.ReadLine();
            
            Player p1 = new Player(name, "Player");

            Item book = new Item(new string[] {"Book", "notebook"}, "book", "Used for reading");
            Item pen = new Item(new string[] {"Pen", "Used to write"}, "Pen", "A ball-point pen");
            p1.PlayerInventory.Put(book);
            p1.PlayerInventory.Put(pen);
            
            Bag p1Bag = new Bag(new string[] {"Bag", "Back to put things in"}, "bag", "You can put things in here");
            p1.PlayerInventory.Put(p1Bag);

            Item textBook = new Item(new string[] {"textbook", "text-book"}, "Text book", "A book for reading");
            p1Bag.Put(textBook);

            location classroom = new location(new string[] {"location", "classroom", "room"}, "classroom", "A place for learning");
            Item tutor = new Item(new string[] {"Jai", "tutor"}, "tutor", "this person teaches things");
            Item whiteboard = new Item(new string[] {"whiteboard", "board"}, "whiteboard", "You can write things on this");
            classroom.locationinventory.Put(whiteboard);
            classroom.locationinventory.Put(tutor);

            p1.changeLocation(classroom);

            location library = new location(new string[] {"location", "library"}, "library", "A place for reading");
            Path mypath = new Path(new string[] {"path", "library-path", "library"}, library, "library-path",  "Hallway connecting library and classroom");
            classroom.addPath(mypath);

            location lockerroom = new location(new string[] {"location", "lockerroom"}, "lockerroom", "lockers are here");
            Path path2 = new Path(new string[] {"path", "lockerroom", "lockerroom-path"},lockerroom, "lockerroom-path",  "Hallway connecting class and lockerrooms");
            classroom.addPath(path2);

            Path mypath2 = new Path(new string[] {"Path", "classroom-path", "classroom"}, classroom, "classroom-path", "Hallway connecting classroom and library");
            library.addPath(mypath2);

            Path mypath3 = new Path(new string[] {"Path", "classroom-path", "classroom"}, classroom, "classroom-path", "Hallway connecting lockerroom and classroom");
            lockerroom.addPath(mypath3);

            CommandProcessor processor = new CommandProcessor(p1);
            GameLoop(processor, p1);
        }

        public static void GameLoop(CommandProcessor processor, Player _p1){
            bool exit = false;
            while(!exit){
                string input = Console.ReadLine();
                if(input == "exit"){
                    exit = true;
                }
                else{
                    if(input != null){
                        Console.WriteLine(processor.Execute(input, _p1));
                    }
                }
            }
        }
    }
}