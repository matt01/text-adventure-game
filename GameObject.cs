using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class GameObject : identifiableObject{
        protected string _description;
        protected string _name;
		protected string[] _ids;
        public GameObject(string[] ids, string name, string desc) : base(new List<string>(ids)){
            _ids = ids;
			_name = name;
			_description = desc;
        }

		public string name{
			get{
				return _name;
			}
		}

		public string ShortDescription{
			get{
				_description = name + " (" + _description + ")";
				return _description;
			}
		}

		public virtual string FullDescription{
			get{
				return _description;
			}
		}
    }
}