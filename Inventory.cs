using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class Inventory : GameObject{
        protected List<Item> _items =  new List<Item>();
        private static string[] _id = new string[] {"Inventory", "Place to put things"};

        public Inventory() : base(_id, "Inventory", "A place to put things"){

        }

        public bool HasItem(string id){
            foreach(Item i in _items){
                if(i.AreYou(id) == true){
                    return true;
                }
            }
            return false;
        }

        public void Put(Item itm){
            _items.Add(itm);
        }

        public Item Take(string id){
            Item i = (Item)Fetch(id);
            Item result = i;
            _items.Remove(i);
            return result;
        }

        public Item Fetch(string id){
            foreach(Item i in _items){
                if(i.AreYou(id) == true){
                    return i;
                }
            }
            return null!;
        }

        public string ItemList{
            get{
                string names = "";
                foreach(Item i in _items){
                    names = names + "\n" + i.name;
                }
                return names;
            }
        }
    }
}