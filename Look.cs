using System;

namespace SwinTopia{
    public class Look : Command{
        private static string[] ids = new string[] {"Look", "LookCommand"};
        public Look() : base(ids){

        }

        public override string Execute(Player p, string[] text)
        {
            string result = "";
            
            if(text[0].ToLower() != "look"){
                return "Error in look input";
            }
            if(text.Length == 1){
                return LookAtIn("location", p);
            }
            if(text.Length != 5 && text.Length != 3 && text.Length != 1){
                return "I don't know how to look like that";
            }
            if(text[1].ToLower() != "at"){
                return "What do you want to look at?";
            }
            if(text.Length == 5){
                IHaveInventory mycontainer = FetchContainer(p, text[4]);
                if(mycontainer == null){
                    return "I can't find the " + text[4];
                }
                
                result = LookAtIn(text[2], mycontainer);
            }
            if(text.Length == 3){
                result = LookAtIn(text[2], p);
            }
            return result;
        }

        public IHaveInventory FetchContainer(Player P, string containerid){
            return (IHaveInventory)P.Locate(containerid);
        }

        public string LookAtIn(string thingid, IHaveInventory container){
            GameObject thing = container.Locate(thingid);
            if(thing == null){
                return "I can't find the " + thingid;
            }

            return thing.FullDescription;
        }
    }
}