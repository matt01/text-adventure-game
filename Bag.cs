using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class Bag : Item, IHaveInventory{
        private readonly Inventory _Inventory = new Inventory();
        public Bag(string[] ids, string name, string desc) : base(ids, name, desc){

        }

        public GameObject Locate(string id){
            if(this.AreYou(id) == true){
                return this;
            }
            return _Inventory.Fetch(id);
        }

        public void Put(Item itm){
            _Inventory.Put(itm);
        }

        public string Name{
            get{
                return _name;
            }
        }

        public override string FullDescription{
            get{
                string things = _Inventory.ItemList;
                return _description + "\nIt contains:" + things;
            }
        }
    }
}