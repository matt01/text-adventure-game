using System;
using System.Collections.Generic;

namespace SwinTopia{
    public class Player : GameObject, IHaveInventory{
        private Inventory _Inventory = new Inventory();
        private string _FullDescription = "You are the Player";

        private location _playerlocation = null!;
    
        public Player(string name, string desc) : base(new string[] {"me", "inventory", "player"}, name, desc){

        }

        public void changeLocation(location newloc){
            _playerlocation = newloc;
        }

        public GameObject Locate(string id){
            if(this.AreYou(id) == true){
                return this;
            }
            GameObject result = _Inventory.Fetch(id);

            if(result == null){
                result = playerlocation.Locate(id);
            }

            if(result == null){
                result = playerlocation.FindPath(id);
            }

            return result;
        }

        public override string FullDescription{
            get{
                string things = _Inventory.ItemList;
                return _FullDescription + "\nYour inventory contains:" + things;
            }
        }

        public Inventory PlayerInventory{
            get{
                return _Inventory;
            }
        }

        public string Name{
            get{
                return _name;
            }
        }

        public location playerlocation{
            get{
                return _playerlocation;
            }
        }
    }
}